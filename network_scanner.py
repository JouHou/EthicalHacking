#!/usr/bin/env python3
"""
Simple network scanner.
USAGE:
'python network_scanner.py -t TARGET_IP_ADDRESS'
or
'python network_scanner.py --target=TARGET_IP_ADDRESS'
"""

import scapy.layers.l2 as layers
import scapy.all as scapy
import argparse


def get_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument("-t", "--target", dest="target_ip_address",
                        help="IP address (or range) whose MAC address you want to know")
    options = parser.parse_args()
    if not options.target_ip_address:
        parser.error("[-] Please specify an IP address, use --help for more info.")
    return options


def scan(ip):
    arp_request = layers.ARP(pdst=ip)
    broadcast = layers.Ether(dst="ff:ff:ff:ff:ff:ff")
    arp_request_broadcast = broadcast/arp_request
    answered_list = scapy.srp(arp_request_broadcast, timeout=1, verbose=False)[0]

    clients_list = []
    for element in answered_list:
        client_dict = {"ip": element[1].psrc, "mac": element[1].hwsrc}
        clients_list.append(client_dict)
    return clients_list


def print_results(results_list):
    print("|{}|\n| {:52}|".format("-"*53, "Found {} hosts for the specified IP range.".format(len(results_list))))
    print("|{}|\n| {:25}| {:25}|\n|{}|".format("-"*53, "IP address", "MAC address", "-"*53))
    for item in results_list:
        print("| {:25}| {:25}|".format(item["ip"], item["mac"]))
    print("|{}|\n| {:25}| {:25}|\n|{}|".format("-"*53, "IP address", "MAC address", "-"*53))


if __name__ == '__main__':
    opts = get_arguments()
    scan_results = scan(opts.target_ip_address)
    print_results(scan_results)
