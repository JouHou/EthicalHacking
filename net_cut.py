#!/usr/bin/env python3
"""
Simple net cutter.
USAGE:
'python net_cut'
or
'python net_cut'
"""
import scapy.layers.l2 as layers
from scapy_http import http
import scapy.all as scapy
import netfilterqueue


def process_packet(packet):
    print(packet)
    packet.drop()


if __name__ == '__main__':
    queue = netfilterqueue.NetfilterQueue()
    queue.bind(0, process_packet)
    queue.run()
