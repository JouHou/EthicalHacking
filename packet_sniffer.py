#!/usr/bin/env python3
"""
Simple packet sniffer.
USAGE:
'python packet_sniffer'
or
'python packet_sniffer'
"""
from scapy_http import http
import scapy.all as scapy


keywords = ["username", "user", "login", "password", "loginname", "pass", "uid", "passw", "userid"]


def sniff(interface):
    scapy.sniff(iface=interface, filter='tcp port 1337', store=False, prn=process_sniffed_packet)


def get_url(packet):
    return packet[http.HTTPRequest].Host + packet[http.HTTPRequest].Path


def get_login_info(packet):
    if packet.haslayer(scapy.Raw):
        payload = packet[scapy.Raw].load
        for kw in keywords:
            if kw.encode() in payload:
                return payload.decode()


def process_sniffed_packet(packet):
    """if packet.haslayer(http.HTTPRequest):
        url = get_url(packet)
        # print("-"*100, "\n[+] HTTP Request URL: ", url.decode())

        login_info = get_login_info(packet)
        if login_info:
            print("\n[+] Possible username and/or password: ", login_info)
            print("[+] Packet source IP: ", packet[scapy.IP].src)
            print("[+] Packet source MAC: ", packet[scapy.Ether].src)
            print("[+] Packet destination IP: ", packet[scapy.IP].dst)
            print("[+] Packet destination MAC: ", packet[scapy.Ether].dst)
    """
    print(packet.show())


if __name__ == '__main__':
    sniff("lo")
