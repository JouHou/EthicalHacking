#!/usr/bin/env python3
"""
Simple MAC address changer.
USAGE: "python mac_changer -i INTERFACE -m MAC_ADDRESS"
or
python mac_changer --interface=INTERFACE --mac=MACADDR
"""

import subprocess
import optparse
import re


def get_arguments():
    parser = optparse.OptionParser()
    parser.add_option("-i", "--interface", dest="interface", help="Interface to change MAC address")
    parser.add_option("-m", "--mac", dest="macaddr", help="New MAC address")
    (options, arguments) = parser.parse_args()
    if not options.interface:
        parser.error("[-] Please specify an interface, use --help for more info.")
    elif not options.macaddr:
        parser.error("[-] Please specify a MAC address, use --help for more info.")
    return options


def change_mac(opts):
    try:
        print("[+] Changing MAC address of {} to {}".format(opts.interface, opts.macaddr))
        subprocess.call(["ifconfig", opts.interface, "down"])
        subprocess.call(["ifconfig", opts.interface, "hw", "ether", opts.macaddr])
        subprocess.call(["ifconfig", opts.interface, "up"])
    except:
        print("[-] Something went wrong, use --help for usage info.")


def check_mac_addr(opts):
    ifconfig_result = subprocess.check_output(["ifconfig", opts.interface])
    macaddr_srch_rslt = re.search(r"\w\w:\w\w:\w\w:\w\w:\w\w:\w\w", ifconfig_result)
    if macaddr_srch_rslt:
        return macaddr_srch_rslt.group(0)
    else:
        print("[-] Could not read MAC address")


if __name__ == '__main__':
    opts = get_arguments()
    curr_mac = check_mac_addr(opts)
    print("[i] Current MAC address of {} is {}.".format(opts.interface, curr_mac))
    if curr_mac:
        change_mac(opts)
        curr_mac = check_mac_addr(opts)
    if curr_mac == opts.macaddr:
        print("[+] MAC address of {} changed successfully to {}.".format(opts.interface, opts.macaddr))
    else:
        print("[-] MAC address could not be changed successfully.")
